# Introduction

$`\LaTeX`$ est langage et un système de composition de documents

<https://www.latex-project.org/>

Il permet la réalisation de tout type de documents :

* articles,
* rapports,
* thèses,
* présentations,
* posters,
* cv,
* livres,
* partitions de musiques, etc.

# Historique

* 1978, création de $`\TeX`$ par Donald Knuth (informaticien, spécialiste en algorithmes, etc.)
* 1983, naissance de $`\LaTeX`$, sur-couche de macro-commandes par Leslie Lamport  

# Pourquoi utiliser LaTeX

* outil libre
* existe sur de nombreuses plateformes
* fournit un rendu typographique de qualité professionnelle
* outil puissant pour les documents complexes, l'édition de formules mathématiques
* assure une bonne compatibilité ascendante et descendante
* document produit versionnable et interfaçable
* robuste
* Turing complet
* n'est pas WYSIWYG

# Installation (pas à l'ordre du jour du TP)

Pour travailler avec $`\LaTeX`$ il faut 
* un éditeur de texte
* une distribution $`\LaTeX`$ (contenant de préférence pdflatex)

Les distributions les plus courantes sont 
* Windows
  - Texlive et MikTex
* Mac
  - MacTex 
* Linux
  - Tex live (installer texlive-full)

# Editeurs

$`\LaTeX`$ n'est pas un logiciel de traitement de texte. Il ne permet pas par exemple de faire de l'analyse syntaxique et ne fournit pas d'interface graphique. Ces outils existent néanmoins soit dans votre éditeur de texte favorit, soit dans des environnement intégrés. 

* éditeurs
  - vi 
  - emacs
  - gedit
  - kile
  - ...

* environnements d'édition
  - LyX
  - Texmaker (linux, windows)
  - Texshop (Mac)
  
* services web
  - Overleaf
  - ShareLaTeX
  - \Bluelatex

# Principe de fontionnement 

L'utilisateur écrit un texte ascii, dans lequel sont codées des instructions de mise en forme, codées en $`\LaTeX`$, dans un fichier généralement d'extension 

```bash
  .tex
```

Le document est compilé et transformé dans un format de type pdf, ps, html, etc. pour être vu avec sa mise en page. 

Par principe, la mise en forme est facilement modifiable car elle dépend du style du document choisi. Le contenu est ainsi facilement réutilisable. 

## exercice

Enregistrer ces lignes dans un fichier [01exemple.tex](TP/01exemple.tex)

```math
  \documentclass{article}
  \begin{document}
  Bonjour !
  \end{document}
```

et compiler. En ligne de commande, lancer la commande 

```bash
  pdflatex 01exemple.tex
```

Avec texmaker, comiler et visualiser en pressant les touches F6 puis F7.



# Compilation

En utilisant pdflatex, un .pdf est généré 

```bash
pdflatex 01exemple.tex
```

en ligne de commande, on obtiens 

```bash
acadiou@plume: [master] ~/ASPICS/Cours_LaTeX/TP$ pdflatex 01exemple.tex
This is pdfTeX, Version 3.14159265-2.6-1.40.16 (TeX Live 2015/Debian) (preloaded format=pdflatex)
 restricted \write18 enabled.
entering extended mode
(./01exemple.tex
LaTeX2 <2016/02/01>
Babel <3.9q> and hyphenation patterns for 81 language(s) loaded.
(/usr/share/texlive/texmf-dist/tex/latex/base/article.cls
Document Class: article 2014/09/29 v1.4h Standard LaTeX document class
(/usr/share/texlive/texmf-dist/tex/latex/base/size10.clo))
No file 01exemple.aux.
[1{/var/lib/texmf/fonts/map/pdftex/updmap/pdftex.map}] (./01exemple.aux) )</usr
/share/texlive/texmf-dist/fonts/type1/public/amsfonts/cm/cmr10.pfb>
Output written on 01exemple.pdf (1 page, 11627 bytes).
Transcript written on 01exemple.log.
```

Sont générés un fichier 
* .pdf, rendu du document pour publication ;
* .aux, fichier auxiliaire permettant l'enregistrement vers les objets pointés (numéros de page, etc.); est utilisé après uen seconde compilation pour placer les citations ou appel des références (numéros de figures, etc.); 
* .log, journal de bord, enregistrant l'ensemble des opérations effectuées par le programme (aide éventuellement en cas d'erreur);

Dans un cas plus général, on trouvera dans un projet de document 
* .tex, le (ou les) fichier(s) source
* .pdf ou .ps, rendu du document pour publication 
* .aux, .toc, .idx pour générer les références (pages, numéros de figures, table des matères, etc.)
* .bib, un fichier contenant la bibliographie, uilisable avec bibtex
* .bbl, contenant les liens vers les références bibliographiques
* .sty, .cls, des fichiers de style ou de classe



# Gestion des erreurs

La compilation avec pdflatex génère donc plusieurs fichiers dont le .pdf (s'il n'y a pas d'erreur). 

Lorsqu'à la compilation une erreur se produit stoppant la production du document, lire le message d'erreur, utiliser l'aide proposée et procéder étape par étape si nécessaire. Parfois, le document .pdf est réalisé, mais incorrect. C'est souvent le cas lorsque l'encodage de caractères spéciaux n'a pas été pris en compte ou lorsque des références sont manquantes ou redondantes. 

 Dans l'exemple précédent, l'environnement est insuffisant lorsqu'on travaille en français (à cause des caractères accentués). 

L'[exemple](TP/01exemplePB.tex) ci-dessous se compile, mais le resultat est incorrect :

```math
  \documentclass{article}
  \begin{document}
  Bonjour à tous !
  \end{document}
```

## exercice 

Générer le document pdf correspondant à l'[exemple](TP/01exemplePB.tex).




Pour prendre en compte les caractères spéciaux, on doit défini le type d'encodage du document. Pour le français, cela peut se faire comme dans l'[exemple](TP/02exemple.tex) ci dessous :

```math
  \documentclass[frenchb]{article}
  \usepackage[utf8]{inputenc}
  \usepackage[T1]{fontenc}
  \usepackage{lmodern}
  \usepackage[a4paper]{geometry}
  \usepackage{babel}
  \begin{document}
  Bonjour à tous !
  \end{document}
```

La première ligne définit le type de document, et appelle l'option de langage. Sont appelés ensuite des options de style, d'environnement, etc. dans un préambule. Le corps du document est décrit entre $`\begin{document}`$ et $`\end{document}`$. 

## exercice 

Le fichier [suivant](TP/02exemplePB.tex) comporte une erreur de syntaxe $`\LaTeX`$. La détecter à la compilation.  



# Encodage 

Si utf8 ne peut pas être utilisé, le remplacer par 
* latin1 pour linux, 
* cp1252 pour Windows,
* applemac pour Mac.

Dans Texmaker, vérifier les options d'encodage (Option, Configurer, Texmaker)

# Caractères accentués

Les caractères spéciaux peuvent être explicitement définis avec le langage $`\LaTeX`$, avec une syntaxe du type $`\ + accent + lettre`$. PAr exemple :

* é s'écrit $`\'e`$
* ç : $`\c{c}`$ 
 

# Types de documents 

* report : petits documents de quelques pages,
* article : article de journaux,
* book : documents en plusieurs parties (livres, thèses, etc.)
* letter : lettre
* beamer : pour faire des présentations

# Sections 

Chaque document, suivant son type, peut être découpé en plus ou moins de sections 
* part
* chapter
* section 
* subsection
* subsubsection
* paragraph
* subparagraph

Ces objets sont numérotés. Pour supprimer l'affichage des numéros, il suffit d'ajouter un $`*`$. Exemple :

* $`\section{Section numérotée}`$
* $`\section*{Section non numérotée}`$

# Titres 

Un titre peut être ajouté et est défini comme dans l'[exemple](TP/03exemple.tex) ci dessous :

```math
\documentclass{article}
\begin{document}
\title{Un titre}
\author{F\'ee Clochette}
\date{\today}
\maketitle
\abstract{Ceci est un r\'esum\'e}
\section{Par une nuit de pleine lune}
Il \'etait une fois \ldots
\end{document}
```

## exercice 

Compiler le [fichier](TP/03exemplePB.tex) de l'exemple. Le modifier et recompiler.



# Environnements

Le corps du document est structuré avec des zones de texte dont la mise en forme est particulière, comme 
* des [listes](TP/liste.tex)
* des [tableaux](TP/tableau.tex)
* des [figures](TP/figures.zip)
* des [expressions mathématiques](TP/math.tex)
* des [références bibliographiques](TP/biblio.zip)
* etc.

L'appel à ces objets commence toujours par 
 $`\begin{environnement}`$ et se fini par $`\end{environnement}`$. 

## exercice 

Manipuler les exemples de chacun de ces environnements. 

# Caractères spéciaux et symboles mathématiques 

[Liste](TP/exemples/A.pdf)

# Beamer 

Beamer est une classe de document permettant de faire des présentations. L'appel à la classe se fait dans l'entête 

```math
\documentclass[11pt,a4paper]{beamer}
```

Les transparents dont définis dans l'environnement $`frame`$.

Compiler un [exemple minimal](TP/01beamer.tex)

De nombreux style (templates) sont disponibles. En choisir un et modifier le document pour le prendre en compte. 

# Exercices tirés d'Overleaf

* [article](TP/overleaf_article/paper.zip)
* [beamer](TP/overleaf_beamer/metropolis.zip)
* [thèse](TP/overleaf_thesis/title-ph-d.zip)
* [poster](TP/overleaf_poster/untitled.zip)
* [cv](TP/overleaf_cv/cv.zip)

# Outils collaboratifs

Le développement d'un document seul ou à plusieurs peut nécessiter de faire un suivi de modifications. Si les services web facilient le développement à plusieurs, il peut être intéressant d'exploiter aussi le versionnement en local. 

Pour cela, des outils comme git, diffpdf et latexdiff sont utiles. 

# exercice

Tester un développement à deux avec ces outils à partir de deux versions d'un même fichier sur un [exemple simple](TP/collaboratif/diff.zip). 



